package repaso_juan;

public class PruebaRama5 {

	public static void main(String[] args) {
			
		// Declaraci�n e inicializaci�n de un array de enteros de tama�o 10
		
		int enteros[] = new int[10];
		int contador = 0;
		
		//Introducci�n de valores en el array y visualizaci�n de los mismos: en cada posici�n el contenido ser� su n�mero de posici�n
		
		for (int i = 0; i < enteros.length; i++) {
			enteros[i] = i;
			System.out.println(enteros[i]);
		}
		
		//Modificaci�n del array y visualizaci�n del mismo: en las posiciones pares del array se modificar� el contenido que tenga por 0
		
		for (int i = 0; i < enteros.length; i++) {
			if (enteros[i]%2 == 0) {
				enteros[i] = 0;
			}
			System.out.println(enteros[i]);
		}
		
		//Visualizaci�n del n�mero de ceros que hay en el array.
				
		for (int i = 0; i < enteros.length; i++) {
			if (enteros[i] == 0) {
				contador++;
			}
		}
		
		System.out.println(contador);
		
		//Visualizaci�n del n�mero de elementos distintos de cero que hay en el array.
		
		contador = 0;
		
		for (int i = 0; i < enteros.length; i++) {
			if (enteros[i] != 0) {
				contador++;
			}
		}
		System.out.println(contador);

	}

}
